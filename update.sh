#!/bin/sh

for i in $(seq 1 10)
  do dd if=/dev/urandom of=./random_$i.dat bs="$i"M count=1 > /dev/null 2>&1
done
